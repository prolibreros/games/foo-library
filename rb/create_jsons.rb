#!/usr/bin/env ruby
# encoding: UTF-8
# coding: UTF-8

require 'active_support/all'
require 'fileutils'
require 'json'

Encoding.default_internal = Encoding::UTF_8

# Variables
content_raw     = []
$hash_story     = {:type => 'story',  :score => 0, :timer => 0,  :words => [], :content => []}
$hash_arcade    = {:type => 'arcade', :score => 0, :timer => 99, :words => [], :content => []}
$length         = 7                         # Extensión de la línea deseada
$total_words    = []                        # Para obtener el total de palabras: no mover.

# Estos solo aplican para el modo historia
$chances            = 3                     # Oportunidades para poder elegir la letra
$min_answers        = 2                     # Mínimas opciones posibles; Ojo: va aumentando y siempre es menor a $max_answers
$max_answers        = 3                     # Máximas opciones posibles; Ojo: va aumentando hasta $max_answers_total
$max_answers_total  = 10                    # Límite de las máximas opciones posibles
$bomb_or_bonus      = 20                    # Probabilidad entre 0 y 20 de aparecer una bomba (=0) o bono (=1); Ojo: va aumentando conforme se acerca al final
$on_line_completed  = 'on_line_completed'   # Cuando la línea ha sido completada
$on_line_correct    = 'on_line_correct'     # Cuando la línea ha sido completada correctamente
$on_line_incorrect  = 'on_line_incorrect'   # Cuando la línea ha sido completada incorrectamente
$on_first_change    = 'on_first_change'     # Cuando es el primer cambio de letra
$on_change          = 'on_change'           # Cuando se cambia de letra
$on_loop            = 'on_loop'             # Cuando se da una nueva vuelta en el cambio de letra
$on_bomb            = 'on_bomb'             # Cuando se da con una bomba
$on_bonus           = 'on_bonus'            # Cuando se da con un bono

# Definiciones

# Elimina tildes a las letras
def transliterate s
    return ActiveSupport::Inflector.transliterate(s.to_s).to_s
end

# Ajusta la línea para cumplir con la extensión requerida
def line_length_adjust line
    # La línea es limpiada y cortada al máximo permitido
    line = line.strip[0..($length - 1)]

    # Obtiene cuántos caracteres faltan para completar la línea
    missing = $length - line.length

    return line + (' ' * missing)
end

# Extrae el texto de los TXT
def extract_text file_name
    content = []
    file = File.open(file_name, 'r:UTF-8')
    file.each do |line|
        content.push(line_length_adjust(line))
    end
    file.close

    return content
end

# Crea el archivo JSON
def create_json file_name
    file = File.new(file_name + '.json', 'w:UTF-8')
    if file_name =~ /story/
        file.puts JSON.pretty_generate($hash_story)
    else
        file.puts JSON.pretty_generate($hash_arcade)
    end
    file.close
end

# Translitera el nombre de los archivos para evitar errores
def transliterar texto, oracion = true, cortar = false
	# Elementos particulares a cambiar
	elementos1 = "ñáàâäéèêëíìîïóòôöúùûü"
	elementos2 = "naaaaeeeeiiiioooouuuu"
	
	# Pone el texto en bajas
	texto = texto.downcase
	
	# Limita el nombre a cinco palabras
    if oracion
	    texto = texto.split(/\s+/)
        if cortar == false
    	    texto = texto[0..4].join("_")
        else
    	    texto = texto.join("_")
        end
    end
	
	# Cambia los elementos particulares
    texto = texto.tr(elementos1, elementos2)
	
	return texto
end

# Limpia el conjunto
def clean_array array

    array = array.join(' ')                                 # Junta palabras con un espacio
                 .gsub(/-\s+/, '')                          # Junta palabras separadas por guion
                 .gsub(/[^A-Za-z0-9ÁÉÍÓÚÜÑáéíóúüñ\s]/, '')  # Elimina todo lo que no sea alfanumérico o espacio
                 .split(' ')                                # Divide palabras cada espacio
                 .uniq                                      # Elimina repetidos
                 .map{|w| w.capitalize}                     # Les añade versal inicial
                 .sort_by{|w| transliterar(w, false)}       # Los ordena alfabéticamente

    return array
end

# Convierte las líneas de texto a objetos para arcade
def string_to_hash_arcade line
    if line.strip.length != 0
        $hash_arcade[:content].push(line)
        $total_words.push(line.strip)
    end
end

# Convierte las líneas de texto a objetos para una historia
def string_to_hash_story index_total, index_actual, line

    # Obtiene las posibles respuestas
    def random_letters index_total, index_actual, correct
        total = $min_answers + rand(($max_answers + 1) - $min_answers)
        $final_pick = []

        # Elige las letras incorrectas
        def random_select c

            # La distribución se basa al del Scrabble: https://es.m.wikipedia.org/wiki/Distribuci%C3%B3n_de_las_letras_en_el_Scrabble#Castellano
            vowels = [
                'a','a','a','a','a',    # x12
                'a','a','a','á','á',
                'á','á',
                'e','e','e','e','e',    # x12
                'e','e','e','é','é',
                'é','é',
                'i','i','i','i','í',    # x6
                'í',
                'o','o','o','o','o',    # x9
                'o','ó','ó','ó',
                'u','u','u','ú','ü']    # x5
            consonants = [
                'b','b',                # x2
                'c','c','c','c',        # x4
                'd','d','d','d','d',    # x5
                'f',                    # x1
                'g','g',                # x2
                'h','h',                # x2
                'j',                    # x1
                'k',                    # x1
                'l','l','l','l',        # x4
                'm','m',                # x2
                'n','n','n','n','n',    # x5
                'ñ',                    # x1
                'p','p',                # x2
                'q',                    # x1
                'r','r','r','r','r',    # x5
                's','s','s','s','s',    # x6
                's',
                't','t','t','t',        # x4
                'v','w','x','y','z']    # x1
            punctuation = [
                '.','.','.','.','.',
                ',',',',',',',',',',
                ';',';',';',':',':',
                ':','¡','¡','¡','¡',
                '!','!','!','!','¿',
                '¿','¿','¿','?','?',
                '?','?','(','(','(',
                ')',')',')','«','»',
                '“','”','-','[',']']

            # Cuando aún no hay nada, se agrega la respuesta correcta
            if $final_pick.length == 0
                $final_pick.push(c)
            # Elección de las letras
            else
                accepted = true
                candidate_type = [*0..($bomb_or_bonus - 1)].sample

                # Cero es bomba
                if candidate_type == 0
                    candidate = 0
                # Uno es bono
                elsif candidate_type == 1
                    candidate = 1
                # El resto es un caracter
                else
                    # Detecta si es vocal, consonante o puntuación
                    if transliterate(c) =~ /\w/
                        if transliterate(c) =~ /[aeiou]/
                            candidate = vowels.sample
                        else
                            candidate = consonants.sample
                        end
                    else
                        candidate = punctuation.sample
                    end
                end

                # Indaga si ya está el candidato presente en el conjunto
                $final_pick.each do |l|
                    if candidate_type > 1 && candidate.downcase == l.to_s.downcase
                        accepted = false
                        break
                    end
                end

                # Si el candidato no está en el conjunto, es aceptado; de lo contrario se repite la operación
                if accepted
                    # Hace mayúscula si la respuesta correcta lo está
                    if candidate_type > 1 && transliterate(c) =~ /[A-Z]/
                        candidate = candidate.upcase
                    end

                    $final_pick.push(candidate)
                else
                    random_select(c)
                end
            end
        end

        # Revisa que el conjunto no empiece con una bomba o un bono
        def check_shuffle array
            if array.first.class == Integer
                check_shuffle(array.shuffle)
            else
                return array
            end
        end

        # Obtiene las posibles respuestas, incluyendo la correcta
        total.times do random_select(correct) end

        # Revuelve la respuesta correcta de las erróneas
        $final_pick = check_shuffle($final_pick.shuffle)

        # Progresivamente va aumentando la cantidad de respuestas erróneas
        reach = ((index_actual.to_f * $max_answers_total.to_f) / index_total.to_f).round

        if $max_answers < reach && reach <= $max_answers_total
            $max_answers = reach                    # Aumenta el máximo posible
            $min_answers = $min_answers + 1         # Aumenta el mínimo posible
            $bomb_or_bonus = $bomb_or_bonus - 1     # Aumenta la posibilidad de que salga bomba o bonus
        end

        return $final_pick
    end

    # Cada línea es un objeto
    hash_line = {
        :visible            => $hash_story[:content].length == 0 ? true : false,
        :on_line_completed  => $on_line_completed,
        :on_line_correct    => $on_line_correct,
        :on_line_incorrect  => $on_line_incorrect,
    }

    # Si no tiene elementos, es un salto de línea
    if line.strip.length == 0
        if index_total != index_actual
            $hash_story[:content].push(hash_line)
        end
    # Si tiene elementos, es una línea
    else
        # La línea tendrá un conjunto con cada letra siendo un objeto
        content_tmp = []

        # Iteración de la línea en cada letra
        line.split('').each do |letter|
            # Si es espacio implica que es un objeto sin propiedades
            if letter == ' '
                content_tmp.push({:type => 0})
            # Objeto con propiedades
            else
                r_letters = random_letters(index_total, index_actual, letter)

                content_tmp.push({
                                    :type               => 1, 
                                    :status             => 'enable',
                                    :correct            => letter,
                                    :picked             => r_letters.first,
                                    :picked_i           => 0,
                                    :letters            => r_letters,
                                    :chances            => $chances,
                                    :loops              => 0,
                                    :on_first_change    => $on_first_change,
                                    :on_change          => $on_change,
                                    :on_loop            => $on_loop,
                                    :on_bomb            => $on_bomb,
                                    :on_bonus           => $on_bonus
                                })
            end
        end

        hash_line[:content] = content_tmp
        $hash_story[:content].push(hash_line)
    end
end

# Va al directorio «txt»
Dir.chdir(File.dirname(__FILE__) + '/../txt')

# Crea los archivos JSON
Dir.glob('*.txt').each do |f|
    begin
        content_raw = extract_text(f)

        if File.basename(f) =~ /story/
            content_raw.each_with_index do |line, i|
                string_to_hash_story(content_raw.length, i, line)
            end

            $total_words = content_raw
            $hash_story[:words] = clean_array($total_words)
        else
            content_raw.each_with_index do |line, i|
                string_to_hash_arcade(line)
            end

            $hash_arcade[:words] = clean_array($total_words)
        end

        create_json(File.basename(f, '.txt'))

        $total_words = []

        puts "Se creó JSON del archivo '#{f}'."
    rescue
        puts "No fue posible convertir el archivo '#{f}'."
    end
end

# Mueve los JSON a la carpeta correspondiente
Dir.glob('*.json').each do |f|
    FileUtils.mv(f, '../json')
end
